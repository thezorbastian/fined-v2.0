-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
application
ADDED from AndroidManifest.xml:5:5
MERGED from com.android.support:appcompat-v7:22.1.1:22:5
MERGED from com.android.support:support-v4:22.1.1:22:5
	android:label
		ADDED from AndroidManifest.xml:8:9
	android:allowBackup
		ADDED from AndroidManifest.xml:6:9
	android:icon
		ADDED from AndroidManifest.xml:7:9
	android:theme
		ADDED from AndroidManifest.xml:9:9
activity#com.thinkdifferent.fined.Splash
ADDED from AndroidManifest.xml:11:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:14:13
	android:label
		ADDED from AndroidManifest.xml:13:13
	android:name
		ADDED from AndroidManifest.xml:12:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:15:9
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:16:13
	android:name
		ADDED from AndroidManifest.xml:16:21
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:18:13
	android:name
		ADDED from AndroidManifest.xml:18:23
activity#com.thinkdifferent.fined.FirstActivity
ADDED from AndroidManifest.xml:22:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:25:13
	android:label
		ADDED from AndroidManifest.xml:24:13
	android:name
		ADDED from AndroidManifest.xml:23:13
intent-filter#android.intent.action.FIRST+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:26:13
action#android.intent.action.FIRST
ADDED from AndroidManifest.xml:27:17
	android:name
		ADDED from AndroidManifest.xml:27:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:29:17
	android:name
		ADDED from AndroidManifest.xml:29:27
meta-data#android.app.default_searchable
ADDED from AndroidManifest.xml:32:13
	android:name
		ADDED from AndroidManifest.xml:33:17
	android:value
		ADDED from AndroidManifest.xml:34:17
activity#com.thinkdifferent.fined.SecondActivity
ADDED from AndroidManifest.xml:38:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:41:13
	android:label
		ADDED from AndroidManifest.xml:40:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:42:13
	android:name
		ADDED from AndroidManifest.xml:39:13
intent-filter#android.intent.action.SECOND+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:43:13
action#android.intent.action.SECOND
ADDED from AndroidManifest.xml:44:17
	android:name
		ADDED from AndroidManifest.xml:44:25
activity#com.thinkdifferent.fined.ThirdActivity
ADDED from AndroidManifest.xml:55:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:57:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:58:13
	android:name
		ADDED from AndroidManifest.xml:56:13
intent-filter#android.intent.action.THIRD+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:59:13
action#android.intent.action.THIRD
ADDED from AndroidManifest.xml:60:17
	android:name
		ADDED from AndroidManifest.xml:60:25
activity#com.thinkdifferent.actionbar.CourtActivity
ADDED from AndroidManifest.xml:66:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:68:13
	android:name
		ADDED from AndroidManifest.xml:67:13
intent-filter#android.intent.action.COURT+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:69:13
action#android.intent.action.COURT
ADDED from AndroidManifest.xml:70:17
	android:name
		ADDED from AndroidManifest.xml:70:25
activity#com.thinkdifferent.actionbar.Contact
ADDED from AndroidManifest.xml:76:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:79:13
	android:label
		ADDED from AndroidManifest.xml:78:13
	android:name
		ADDED from AndroidManifest.xml:77:13
intent-filter#android.intent.action.CONTACT+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:80:13
action#android.intent.action.CONTACT
ADDED from AndroidManifest.xml:81:17
	android:name
		ADDED from AndroidManifest.xml:81:25
activity#com.thinkdifferent.actionbar.About
ADDED from AndroidManifest.xml:87:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:90:13
	android:label
		ADDED from AndroidManifest.xml:89:13
	android:name
		ADDED from AndroidManifest.xml:88:13
intent-filter#android.intent.action.ABOUT+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:91:13
action#android.intent.action.ABOUT
ADDED from AndroidManifest.xml:92:17
	android:name
		ADDED from AndroidManifest.xml:92:25
activity#com.thinkdifferent.support.SearchResults
ADDED from AndroidManifest.xml:99:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:101:13
	android:label
		ADDED from AndroidManifest.xml:102:13
	android:name
		ADDED from AndroidManifest.xml:100:13
intent-filter#android.intent.action.SEARCH+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:103:13
action#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:104:17
	android:name
		ADDED from AndroidManifest.xml:104:25
meta-data#android.app.searchable
ADDED from AndroidManifest.xml:108:13
	android:resource
		ADDED from AndroidManifest.xml:110:17
	android:name
		ADDED from AndroidManifest.xml:109:17
activity#com.thinkdifferent.support.SearchDisplayActivity
ADDED from AndroidManifest.xml:113:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:115:13
	android:label
		ADDED from AndroidManifest.xml:116:13
	android:name
		ADDED from AndroidManifest.xml:114:13
intent-filter#android.intent.action.DISPLAYRESULT+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:117:13
action#android.intent.action.DISPLAYRESULT
ADDED from AndroidManifest.xml:118:17
	android:name
		ADDED from AndroidManifest.xml:118:25
provider#com.thinkdifferent.support.provider
ADDED from AndroidManifest.xml:124:9
	android:exported
		ADDED from AndroidManifest.xml:127:13
	android:authorities
		ADDED from AndroidManifest.xml:126:13
	android:name
		ADDED from AndroidManifest.xml:125:13
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:appcompat-v7:22.1.1:20:5
MERGED from com.android.support:support-v4:22.1.1:20:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
