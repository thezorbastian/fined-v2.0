package com.thinkdifferent.actionbar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.thinkdifferent.fined.R;

public class Contact extends AppCompatActivity {

    TextView hyperlink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        hyperlink = (TextView)findViewById(R.id.hyperlink);
        hyperlink.setClickable(true);
        hyperlink.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "All information given is as provided by website of <a href='http://trafficpolicemumbai.maharashtra.gov.in/Home/Home.aspx'> Mumbai Traffic Police </a>";
        hyperlink.setText(Html.fromHtml(text));
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}