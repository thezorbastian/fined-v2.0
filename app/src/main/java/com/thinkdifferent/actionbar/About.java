package com.thinkdifferent.actionbar;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.thinkdifferent.fined.R;

/**
 * Created by thezorbastian on 7/5/2015.
 */
public class About extends AppCompatActivity {

    TextView link1,link2,link3,link4,link5,link6;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        link1 = (TextView) findViewById(R.id.links1);
        link1.setClickable(true);
        link1.setMovementMethod(LinkMovementMethod.getInstance());

        link2 = (TextView) findViewById(R.id.links2);
        link2.setClickable(true);
        link2.setMovementMethod(LinkMovementMethod.getInstance());

        link3 = (TextView) findViewById(R.id.links3);
        link3.setClickable(true);
        link3.setMovementMethod(LinkMovementMethod.getInstance());

        link4 = (TextView) findViewById(R.id.links4);
        link4.setClickable(true);
        link4.setMovementMethod(LinkMovementMethod.getInstance());

        link5 = (TextView)findViewById(R.id.links5);
        link5.setClickable(true);
        link5.setMovementMethod(LinkMovementMethod.getInstance());

        link6 = (TextView) findViewById(R.id.links6);
        link6.setClickable(true);
        link6.setMovementMethod(LinkMovementMethod.getInstance());


        try {
            this.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            String text = "<a href='fb://profile/100001060134707'> Clyde Mendonca </a>";
            link1.setText(Html.fromHtml(text));

            String text2 = "<a href='fb://profile/100000139359058'> Vikrant Fernandes </a>";
            link2.setText(Html.fromHtml(text2));

            String text3 = "<a href='fb://profile/100000869772179'> Liston Mascarenhas </a>";
            link3.setText(Html.fromHtml(text3));

            String text4 = "<a href='fb://profile/1395588009'> Alfred Johnson </a>";
            link4.setText(Html.fromHtml(text4));

            String text5 = "<a href='https://plus.google.com/117630760959594474616/posts'> Lisa D'mello </a>";
            link5.setText(Html.fromHtml(text5));

            String text6 = "<a href='fb://profile/526268449'> Mervin Pinto </a>";
            link6.setText(Html.fromHtml(text6));

        }catch (PackageManager.NameNotFoundException e) {
            String text = "<a href='https://www.facebook.com/clyde.mendonca'> Clyde Mendonca </a>";
            link1.setText(Html.fromHtml(text));

            String text2 = "<a href='https://www.facebook.com/vikrant.zorba'> Vikrant Fernandes </a>";
            link2.setText(Html.fromHtml(text2));

            String text3 = "<a href='https://www.facebook.com/th3comedian'> Liston Mascarenhas </a>";
            link3.setText(Html.fromHtml(text3));

            String text4 = "<a href='https://www.facebook.com/alfred.johnson.925'> Alfred Johnson </a>";
            link4.setText(Html.fromHtml(text4));

            String text5 = "<a href='https://plus.google.com/117630760959594474616/posts'> Lisa D'mello </a>";
            link5.setText(Html.fromHtml(text5));

            String text6 = "<a href='https://www.facebook.com/merv28011994'> Mervin Pinto </a>";
            link6.setText(Html.fromHtml(text6));
            //Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
