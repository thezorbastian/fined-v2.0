package com.thinkdifferent.actionbar;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.thinkdifferent.fined.R;
import com.thinkdifferent.support.MapSupport;


public class CourtActivity extends AppCompatActivity {

    //===============================================================
    // VIEWS
    //===============================================================

    ListView lv;

    //===============================================================
    // VARIABLES
    //===============================================================

    Context c;

    //===============================================================
    // METHODS
    //===============================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_court);
        c = this;
        this.setTitle("Magistrate Courts");
        lv = (ListView) findViewById(R.id.list);

        String[] courts = new String[]{"Azad Maidan", "Borivali", "Andheri", "Dadar", "Charni Road", "Bandra", "Kurla", "Mulund", "Mazgaon"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, courts);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int itemPosition = position;
                MapSupport obj2 = new MapSupport(itemPosition, c);
                }
        });
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
}