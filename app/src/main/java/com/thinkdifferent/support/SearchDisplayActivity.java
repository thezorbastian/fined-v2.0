package com.thinkdifferent.support;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.app.ActionBar.LayoutParams;
import android.widget.Toast;

import com.thinkdifferent.actionbar.Contact;
import com.thinkdifferent.actionbar.CourtActivity;
import com.thinkdifferent.fined.R;

import java.lang.reflect.Field;

/**
 * Created by thezorbastian on 06-06-2015.
 */
public class SearchDisplayActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ll;
    TextView tv;
    Button bt;
    Context context;
    Uri something;
    DBclass dbhelper;
    String offencelist[];
    int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        ll = (LinearLayout) findViewById(R.id.linlay);
        Intent intent = getIntent();
        something = intent.getData();
        String id = something.getLastPathSegment();

        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
        }

        dbhelper = new DBclass(SearchDisplayActivity.this);
        offencelist = new String[dbhelper.getdata(id).length];
        offencelist = dbhelper.getdata(id);


        bt = new Button(this);
        bt.setText(offencelist[0]);
        bt.setOnClickListener(this);
        bt.setBackgroundResource(R.drawable.button);
        bt.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        tv = new TextView(this);
        tv.setText("PENALTY\n" + offencelist[2] + "\n\nSECTION\n"
                + offencelist[1]);
        tv.setVisibility(View.GONE);
        tv.setBackgroundResource(R.drawable.textview);
        tv.setGravity(Gravity.CENTER);
        tv.setPadding(5, 5, 5, 5);

        ll.addView(bt);
        ll.addView(tv);

    }

    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_first, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                return true;
            case R.id.about:
                return true;
            case R.id.contact:
                Intent intent97 = new Intent(SearchDisplayActivity.this, Contact.class);
                startActivity(intent97);
                finish();
                return true;
            case R.id.rto:
                MapSupport obj = new MapSupport(99, this);
                break;
            case R.id.courts:
                Intent intent = new Intent(SearchDisplayActivity.this, CourtActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    public void onClick(View v) {
        if (tv.getVisibility() == View.GONE)
        {tv.setVisibility(View.VISIBLE);}
        else
        {tv.setVisibility(View.GONE);}
    }
}
