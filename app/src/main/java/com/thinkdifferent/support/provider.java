package com.thinkdifferent.support;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

/**
 * Created by thezorbastian on 06-06-2015.
 */
public class provider extends ContentProvider {

    private static final String AUTHORITY = "com.thinkdifferent.fined.provider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/laws");

    private DBclass dbHelper = null;

    private static final int ALL_ROWS = 1;
    private static final int SINGLE_ROW = 2;
    private static final int SEARCH = 3;

    public static final String KEY_ID = "_id";
    public static final String KEY_OFFENCE = "offence";

    UriMatcher mUriMatcher = buildUriMatcher();
    private UriMatcher buildUriMatcher(){
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, ALL_ROWS);
        uriMatcher.addURI(AUTHORITY, "laws", SEARCH);
        uriMatcher.addURI(AUTHORITY, "laws/#", SINGLE_ROW);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DBclass(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String query = uri.getLastPathSegment();
        Cursor c = null;
        switch (mUriMatcher.match(uri)) {
            case ALL_ROWS:
                // do nothing
                c = dbHelper.getAlloffences(selectionArgs);
                break;
            case SINGLE_ROW:
                String id = uri.getLastPathSegment();
                c = dbHelper.getOffence(id);
                break;
            case SEARCH:
                c = dbHelper.getAlloffences(selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (mUriMatcher.match(uri)) {
            case ALL_ROWS:
                return "vnd.android.cursor.dir/vnd.com.thinkdifferent.fined.contentprovider.laws";
            case SINGLE_ROW:
                return "vnd.android.cursor.item/vnd.com.thinkdifferent.fined.contentprovider.laws";
            case SEARCH:
                return SearchManager.SUGGEST_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
