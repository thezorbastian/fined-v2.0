package com.thinkdifferent.support;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Toast;

import com.thinkdifferent.fined.R;

import java.util.List;

/**
 * Created by thezorbastian on 16-05-2015.
 */
public class MapSupport {

    Context context;
    public MapSupport(int loc,Context c){
        this.context = c;
        Uri location = null;
        if (loc == 99){
            location = Uri.parse(context.getResources().getString(R.string.rto_offices));
        }
        else if (loc == 0){
            location = Uri.parse(context.getResources().getString(R.string.azadmaidan));
        }
        else if (loc == 1){
            location = Uri.parse(context.getResources().getString(R.string.borivali));
        }
        else if (loc == 2){
            location = Uri.parse(context.getResources().getString(R.string.andheri));
        }
        else if (loc == 3){
            location = Uri.parse(context.getResources().getString(R.string.dadar));
        }
        else if (loc == 4){
            location = Uri.parse(context.getResources().getString(R.string.charni_rd));
        }
        else if (loc == 5){
            location = Uri.parse(context.getResources().getString(R.string.bandra));
        }
        else if (loc == 6){
            location = Uri.parse(context.getResources().getString(R.string.kurla));
        }
        else if (loc == 7){
            location = Uri.parse(context.getResources().getString(R.string.mulund));
        }
        else if (loc == 8){
            location = Uri.parse(context.getResources().getString(R.string.mazgaon));
        }
        try {
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
            PackageManager packmang = context.getPackageManager();
            List<ResolveInfo> activities = packmang.queryIntentActivities(mapIntent, 0);
            boolean isIntentSafe = activities.size() > 0;
            if (isIntentSafe) {
                context.startActivity(mapIntent);
            }
        }catch (Exception e){
            Toast.makeText(context, "Cell phone does not support Map Activities!", Toast.LENGTH_LONG).show();
        }
    }

}
