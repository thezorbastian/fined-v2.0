package com.thinkdifferent.support;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by thezorbastian on 29-05-2015.
 */
public class RawData extends Activity {

    JSONArray jsonArray1,jsonArray2,jsonArray3,jsonArray4,jsonArray5,jsonArray6;
    Context c;
    public RawData(Context cont){
        this.c = cont;
        try{
            InputStream is = c.getAssets().open("parking.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonArray1 = new JSONArray(new String(buffer));

            InputStream is2 = c.getAssets().open("driving.txt");
            int size2 = is2.available();
            byte[] buffer2 = new byte[size2];
            is2.read(buffer2);
            is2.close();
            jsonArray2 = new JSONArray(new String(buffer2));

            InputStream is3 = c.getAssets().open("documents.txt");
            int size3 = is3.available();
            byte[] buffer3 = new byte[size3];
            is3.read(buffer3);
            is3.close();
            jsonArray3 = new JSONArray(new String(buffer3));

            InputStream is4 = c.getAssets().open("pollution.txt");
            int size4 = is4.available();
            byte[] buffer4 = new byte[size4];
            is4.read(buffer4);
            is4.close();
            jsonArray4 = new JSONArray(new String(buffer4));

            InputStream is5 = c.getAssets().open("vehicles.txt");
            int size5 = is5.available();
            byte[] buffer5 = new byte[size5];
            is5.read(buffer5);
            is5.close();
            jsonArray5 = new JSONArray(new String(buffer5));

            InputStream is6 = c.getAssets().open("goods.txt");
            int size6 = is6.available();
            byte[] buffer6 = new byte[size6];
            is6.read(buffer6);
            is.close();
            jsonArray6 = new JSONArray(new String(buffer6));

        }catch(Exception e){
            Toast.makeText(this, "Error reading data from source folder!", Toast.LENGTH_LONG).show();
        }
    }

    public ArrayList<JSONArray> loadData(){

        ArrayList<JSONArray> list = new ArrayList<JSONArray>(6);
        list.add(jsonArray1);
        list.add(jsonArray2);
        list.add(jsonArray3);
        list.add(jsonArray4);
        list.add(jsonArray5);
        list.add(jsonArray6);
        return list;
    }

}
