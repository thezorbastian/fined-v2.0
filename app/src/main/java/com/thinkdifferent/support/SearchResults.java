package com.thinkdifferent.support;

import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.thinkdifferent.fined.R;

/**
 * Created by thezorbastian on 06-06-2015.
 */
public class SearchResults extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    ListView mLV;
    SimpleCursorAdapter mCursorAdapter;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        mLV = (ListView)findViewById(R.id.lv_offence);

        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getApplicationContext(), SearchDisplayActivity.class);
                Uri data = ContentUris.withAppendedId(provider.CONTENT_URI, id);
                it.setData(data);
                startActivity(it);
                finish();
            }
        });

        mCursorAdapter = new SimpleCursorAdapter(getBaseContext(),
                android.R.layout.simple_list_item_1,
                null,
                new String[] { SearchManager.SUGGEST_COLUMN_TEXT_1 },
                new int[] { android.R.id.text1 }, 0);
        mLV.setAdapter(mCursorAdapter);
        parseIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        parseIntent(getIntent());
    }

    private void parseIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String searchQuery = intent.getStringExtra(SearchManager.QUERY);
            Bundle args = new Bundle();
            args.putString("query", searchQuery);
            getLoaderManager().restartLoader(1, args, this);
        }
        else if(Intent.ACTION_VIEW.equals(intent.getAction()))
        {
            Intent mIntent = new Intent(getApplicationContext(), SearchDisplayActivity.class);
            mIntent.setData(intent.getData());
            startActivity(mIntent);
            finish();
        }
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = provider.CONTENT_URI;
        return new CursorLoader(getBaseContext(), uri, null, null , new String[]{args.getString("query")}, null);
    }

    public void onLoadFinished(Loader<Cursor> arg0, Cursor c) {
        mCursorAdapter.swapCursor(c);
    }

    public void onLoaderReset(Loader<Cursor> arg0) {
    }

}
