package com.thinkdifferent.support;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by thezorbastian on 24-05-2015.
 */
public class DBclass extends SQLiteOpenHelper {

    Context c;
    protected static final int DATABASE_VERSION = 3;
    protected static final String DATABASE_NAME = "offences";
    protected static final String TABLE_NAME = "laws";

    public static final String KEY_ID = "_id";
    public static final String KEY_OFFENCE = "offence";
    public static final String KEY_FINE = "fine";
    public static final String KEY_SECTION = "section";
    public static final String KEY_carORbike = "carORbike";
    public static final String KEY_CATEGORY = "category";
    DBclass dbclass = null;
    private HashMap<String, String> mAliasMap;

    public DBclass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.c = context;

        // This HashMap is used to map table fields to Custom Suggestion fields
        mAliasMap = new HashMap<String, String>();

        // Unique id for the each Suggestions ( Mandatory )
        mAliasMap.put("_ID", KEY_ID + " as " + "_id");

        // Text for Suggestions ( Mandatory )
        mAliasMap.put(SearchManager.SUGGEST_COLUMN_TEXT_1, KEY_OFFENCE + " as "
                + SearchManager.SUGGEST_COLUMN_TEXT_1);

        // This value will be appended to the Intent data on selecting an item
        // from Search result or Suggestions ( Optional )
        mAliasMap.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, KEY_ID
                + " as " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME + "'");
        String CREATE_OFFENCE_TABLE = "CREATE TABLE "
                + TABLE_NAME + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_OFFENCE + " VARCHAR, " + KEY_SECTION + " VARCHAR, "
                + KEY_FINE + " VARCHAR, " + KEY_carORbike + " VARCHAR, " + KEY_CATEGORY + " VARCHAR" + ")";

        db.execSQL(CREATE_OFFENCE_TABLE);

        addoffence(db);
    }

    public void addoffence(SQLiteDatabase db){
//        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        ArrayList<JSONArray> list;
        RawData dataobj = new RawData(c);
        list = dataobj.loadData();
        try {
            for(int i = 0; i < list.size(); i++) {
                for (int j = 0; j < list.get(i).length(); j++){
                    JSONObject obj = (JSONObject) list.get(i).get(j);
                    values.put(KEY_OFFENCE,obj.optString("offence"));
                    values.put(KEY_SECTION,obj.optString("section"));
                    values.put(KEY_FINE,obj.optString("fine"));
                    values.put(KEY_carORbike,obj.optString("corb"));
                    values.put(KEY_CATEGORY,obj.optString("category"));
                    db.insert(TABLE_NAME, null, values);
                }
            }
        }catch(Exception e){
            Toast.makeText(c, "Database error. Please reinstall app!", Toast.LENGTH_LONG).show();
        }
        //db.close();
    }

    public String[] getdata(String parameter_offence,String parameter_carORbike){

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_CATEGORY + " = " + "'" + parameter_offence + "'" +
                " AND (" + KEY_carORbike + " LIKE " + "'" + parameter_carORbike + "%'" +
                " OR " + KEY_carORbike + " LIKE " + "'%" + parameter_carORbike + "')";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        String[] data = new String[cursor.getCount()*(cursor.getColumnCount()-1)];
        int j = 0;
        if (cursor.moveToFirst()) {
            do {
                for(int i = 1;i < cursor.getColumnCount();i++,j++){
                    data[j] = cursor.getString(i);
                }
            } while (cursor.moveToNext());
        }
        //db.close();
        return data;
    }

    public String[] getdata(String id){
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + id;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        String data[] = new String[cursor.getColumnCount()-1];
        if(cursor.moveToFirst()) {
            for (int j=0,i = 1; i < cursor.getColumnCount();j++, i++) {
                data[j] = cursor.getString(i);
            }
        }
        return data;
    }

    public Cursor getOffence(String id) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);
        Cursor c = queryBuilder.query(getReadableDatabase(), new String[] {
                        "_id", "name" }, "_id = ?", new String[] { id }, null, null,
                null, "1");
        return c;
    }

    public Cursor getAlloffences(String[] selectionArgs) {
        String selection = KEY_OFFENCE + " like ? ";

        if (selectionArgs != null) {
            selectionArgs[0] = "%" + selectionArgs[0] + "%";
        }
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setProjectionMap(mAliasMap);

        queryBuilder.setTables(TABLE_NAME);

        Cursor c = queryBuilder.query(getReadableDatabase(), new String[] {
                        "_ID", SearchManager.SUGGEST_COLUMN_TEXT_1,
                        SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID }, selection,
                selectionArgs, null, null, KEY_OFFENCE + " asc ", "10");
        return c;
    }

    public int getOffenceCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_NAME + "'");
    }
}