package com.thinkdifferent.fined;

import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thinkdifferent.actionbar.About;
import com.thinkdifferent.actionbar.Contact;
import com.thinkdifferent.actionbar.CourtActivity;
import com.thinkdifferent.support.DBclass;
import com.thinkdifferent.support.MapSupport;


import java.lang.reflect.Field;


public class ThirdActivity extends AppCompatActivity implements View.OnClickListener {

    //===============================================================
    // VIEWS
    //===============================================================

    //private TextView textView;
    private LinearLayout ll;
    private Button[] bt;
    private Button bt_towing2,bt_towing4,bt_towing6;
    private TextView[] tv;
    private TextView towing,towing2,towing4,towing6;

    //===============================================================
    // VARIABLES
    //===============================================================

    private String carORbike, offenceRelatedto;
    private String[] rows;
    private String[] offence_list,section_list,fine_list;
    DBclass db;

    //===============================================================
    // METHODS
    //===============================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Bundle d = this.getIntent().getExtras();
        carORbike = d.getString("carORbike");
        offenceRelatedto = d.getString("offenceRelatedto");

        if(offenceRelatedto.equals("parking")){
            this.setTitle("Parking");
        }else if(offenceRelatedto.equals("pollution")){
            this.setTitle("Pollution");
        }else if(offenceRelatedto.equals("driving")){
            this.setTitle("Driving");
        }else if(offenceRelatedto.equals("vehicles")){
            this.setTitle("Vehicles");
        }else if(offenceRelatedto.equals("documents")){
            this.setTitle("Documents");
        }else if(offenceRelatedto.equals("goods")){
            this.setTitle("Loading of Goods");
        }

        ll = (LinearLayout)findViewById(R.id.linlay);
        //Shifting menu options to overflow in action bar
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            //checks if user phone has hardware menu button or virtual:
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                //hardware button found, moving all menu items to overflow in action bar
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }


        db = new DBclass(ThirdActivity.this);
        rows = new String[db.getdata(offenceRelatedto,carORbike).length];
        rows = db.getdata(offenceRelatedto,carORbike);
        offence_list = new String[rows.length/5];
        section_list = new String[rows.length/5];
        fine_list = new String[rows.length/5];

        for(int i=0,j=0;i<rows.length;j++,i=i+5){
            offence_list[j] = rows[i];
            section_list[j] = rows[i+1];
            fine_list[j] = rows[i+2];
        }

        bt = new Button[offence_list.length];
        tv = new TextView[offence_list.length];

        for (int i = 0; i < offence_list.length; i++) {

            bt[i] = new Button(this);
            bt[i].setText(offence_list[i]);
            bt[i].setOnClickListener(this);
            bt[i].setTag(i);
            bt[i].setBackgroundResource(R.drawable.button);
            bt[i].setTop(20);
            if (i % 2 == 0) {
                bt[i].setLayoutParams(new LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));
            } else
                bt[i].setLayoutParams(new LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));

            tv[i] = new TextView(this);
            tv[i].setText("PENALTY\n₹" +  fine_list[i] + "\n\nSECTION\n"
                    + section_list[i]);
            tv[i].setVisibility(View.GONE);
            tv[i].setBackgroundResource(R.drawable.textview);
            tv[i].setGravity(Gravity.CENTER);
            tv[i].setPadding(5, 5, 5, 5);

            ll.addView(bt[i]);
            ll.addView(tv[i]);
        }

    }


    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_first, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_first, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                Intent back = NavUtils.getParentActivityIntent(this);
                back.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, back);
                return true;
            case R.id.action_search:
                return true;
            case R.id.about:
                Intent intent81 = new Intent(ThirdActivity.this, About.class);
                startActivity(intent81);
                return true;
            case R.id.contact:
                Intent intent97 = new Intent(ThirdActivity.this, Contact.class);
                startActivity(intent97);
                return true;
            case R.id.rto:
                MapSupport obj = new MapSupport(99, this);
                break;
            case R.id.courts:
                Intent intent = new Intent(ThirdActivity.this, CourtActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int i = Integer.parseInt((v.getTag().toString()));
        if (tv[i].getVisibility() == View.GONE)
        {tv[i].setVisibility(View.VISIBLE);}
        else if(tv[i].getVisibility() == View.VISIBLE)
        {tv[i].setVisibility(View.GONE);}

    }
}
