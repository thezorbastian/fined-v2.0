package com.thinkdifferent.fined;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by thezorbastian on 07-06-2015.
 */
public class Splash extends Activity {
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.splash);

        try {
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(2000);
                    } catch (Exception ex) {

                    } finally {
                        Intent start = new Intent(Splash.this, FirstActivity.class);
                        startActivity(start);
                    }
                }
            };
            timer.start();
        }catch (Exception ex){
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    public void onPause(){
        super.onPause();
        finish();
    }
}
