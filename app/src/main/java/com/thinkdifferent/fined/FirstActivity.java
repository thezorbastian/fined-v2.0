package com.thinkdifferent.fined;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import com.thinkdifferent.actionbar.About;
import com.thinkdifferent.actionbar.Contact;
import com.thinkdifferent.actionbar.CourtActivity;
import com.thinkdifferent.support.MapSupport;

import java.lang.reflect.Field;


public class FirstActivity extends AppCompatActivity {

    //===============================================================
    // VIEWS
    //===============================================================

    private ImageButton btBike, btCar;
    private SearchView searchView;

    //===============================================================
    // VARIABLES
    //===============================================================

    private String carORbike;
    private static int x = 0;
    Intent it;

    //===============================================================
    // METHODS
    //===============================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

//        Shifting menu options to overflow in action bar
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            //checks if user phone has hardware menu button or virtual:
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                //hardware button found, moving all menu items to overflow in action bar
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }


        //loading animations:
        final Animation translate = AnimationUtils.loadAnimation(this, R.anim.trans);
        final Animation translate2 = AnimationUtils.loadAnimation(this, R.anim.trans2);
        Animation translate3 = AnimationUtils.loadAnimation(this, R.anim.trans3);
        Animation translate4 = AnimationUtils.loadAnimation(this, R.anim.trans4);

        //Creating handler to add delay before next activity starts:
        final Handler handler = new Handler();

        //setting up the buttons:
        btBike = (ImageButton) findViewById(R.id.bt_bike);
        btCar = (ImageButton) findViewById(R.id.bt_car);

        //Initiating first animation:
        btBike.startAnimation(translate3);
        btCar.startAnimation(translate4);

        btBike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carORbike = "bike";
                x = 1;
                final Bundle b = new Bundle();
                b.putString("cORb", carORbike);
                btBike.startAnimation(translate);
                btCar.startAnimation(translate2);
                translate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        it = new Intent(FirstActivity.this, SecondActivity.class);
                        it.putExtras(b);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        btCar.setVisibility(View.GONE);
                        btBike.setVisibility(View.GONE);
                        startActivity(it);
                    }
                });
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent it = new Intent(FirstActivity.this, SecondActivity.class);
//                        it.putExtras(b);
//                        startActivity(it);
//                    }
//                }, 400);//350 milli seconds before next activity starts
            }
        });

        btCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carORbike = "car";
                x = 2;
                final Bundle b = new Bundle();
                b.putString("cORb", carORbike);
                btBike.startAnimation(translate2);
                btCar.startAnimation(translate);

                translate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        it = new Intent(FirstActivity.this, SecondActivity.class);
                        it.putExtras(b);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        btCar.setVisibility(View.GONE);
                        btBike.setVisibility(View.GONE);
                        startActivity(it);
                    }
                });
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent it = new Intent(FirstActivity.this, SecondActivity.class);
//                        it.putExtras(b);
//                        startActivity(it);
//                    }
//                }, 400);
            }
        });

    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        searchView.setQuery("", false);
        searchView.setIconified(true);
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void onResume() {
        super.onResume();
        btCar.setVisibility(View.VISIBLE);
        btBike.setVisibility(View.VISIBLE);
        //on Activity resume loading animation xml files and image buttons:
        final Animation translate3 = AnimationUtils.loadAnimation(this, R.anim.trans3);
        final Animation translate4 = AnimationUtils.loadAnimation(this, R.anim.trans4);
        btBike = (ImageButton) findViewById(R.id.bt_bike);
        btCar = (ImageButton) findViewById(R.id.bt_car);

        //x value saved in buttton click:
        if (x == 2) {
            btBike.startAnimation(translate3);
            btCar.startAnimation(translate4);
        } else if (x == 1) {
            btBike.startAnimation(translate4);
            btCar.startAnimation(translate3);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_first, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        SearchView.SearchAutoComplete autoCompleteTextView = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        if (autoCompleteTextView != null) {
            autoCompleteTextView.setDropDownBackgroundDrawable(getResources().getDrawable(R.drawable.abc_popup_background_mtrl_mult));
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                return true;
            case R.id.about:
                Intent intent81 = new Intent(FirstActivity.this, About.class);
                startActivity(intent81);
                return true;
            case R.id.contact:
                Intent intent95 = new Intent(FirstActivity.this, Contact.class);
                startActivity(intent95);
                return true;
            case R.id.rto:
                MapSupport obj = new MapSupport(99, this);
                break;
            case R.id.courts:
                Intent intent = new Intent(FirstActivity.this, CourtActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
