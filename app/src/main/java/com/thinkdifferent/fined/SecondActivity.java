package com.thinkdifferent.fined;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.support.v7.widget.SearchView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.thinkdifferent.actionbar.About;
import com.thinkdifferent.actionbar.Contact;
import com.thinkdifferent.actionbar.CourtActivity;
import com.thinkdifferent.support.MapSupport;

import java.lang.reflect.Field;

/**
 * Created by thezorbastian on 05-05-2015.
 */
public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    //===============================================================
    // VIEWS
    //===============================================================

    private ImageButton parking, driving, documents, pollution, vehicles, loading_goods;
    private SearchView searchView;

    //===============================================================
    // VARIABLES
    //===============================================================

    private String carORbike, offenceRelatedto;
    private String temp_carORbike;

    //===============================================================
    // METHODS
    //===============================================================

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //Getting car or bike:
        Bundle b = this.getIntent().getExtras();
        if(b != null) {
            carORbike = b.getString("cORb");
        }else{
            carORbike = savedInstanceState.getString("temp_cORb");
        }


        //Shifting menu options to overflow in action bar
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            //checks if user phone has hardware menu button or virtual:
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                //hardware button found, moving all menu items to overflow in action bar
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }

        //NOTE:Button is temporary, ImageButton is final goal.
        parking = (ImageButton) findViewById(R.id.bt1);
        driving = (ImageButton) findViewById(R.id.bt2);
        documents = (ImageButton) findViewById(R.id.bt3);
        pollution = (ImageButton) findViewById(R.id.bt4);
        vehicles = (ImageButton) findViewById(R.id.bt5);
        loading_goods = (ImageButton) findViewById(R.id.bt6);
        //Following snippet sets images depending on user choice
        //To be implemented after image problem is sorted out
        documents.setBackgroundResource(R.drawable.docs);
        if(carORbike.equals("car")){
            vehicles.setBackgroundResource(R.drawable.vehicles);
            loading_goods.setBackgroundResource(R.drawable.goods_car);
            driving.setBackgroundResource(R.drawable.drivingcar);
            pollution.setBackgroundResource(R.drawable.pollutioncar);
            parking.setBackgroundResource(R.drawable.parkingcar);
        }
        else{
            vehicles.setBackgroundResource(R.drawable.vehiclebike);
            loading_goods.setBackgroundResource(R.drawable.goodsbike);
            driving.setBackgroundResource(R.drawable.drivingbike);
            pollution.setBackgroundResource(R.drawable.pollutionbike);
            parking.setBackgroundResource(R.drawable.parkingbike);
        }
        parking.setOnClickListener(this);
        driving.setOnClickListener(this);
        documents.setOnClickListener(this);
        pollution.setOnClickListener(this);
        vehicles.setOnClickListener(this);
        loading_goods.setOnClickListener(this);

    }


    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        searchView.setQuery("", false);
        searchView.setIconified(true);
        temp_carORbike = carORbike;
        outState.putString("temp_cORb",temp_carORbike);
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_first, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        SearchView.SearchAutoComplete autoCompleteTextView = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        if (autoCompleteTextView != null) {
            autoCompleteTextView.setDropDownBackgroundDrawable(getResources().getDrawable(R.drawable.abc_popup_background_mtrl_mult));
        }

        //getMenuInflater().inflate(R.menu.menu_first, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                return true;
            case R.id.about:
                Intent intent81 = new Intent(SecondActivity.this, About.class);
                startActivity(intent81);
                return true;
            case R.id.contact:
                Intent intent95 = new Intent(SecondActivity.this, Contact.class);
                startActivity(intent95);
                return true;
            case R.id.rto:
                MapSupport obj = new MapSupport(99, this);
                break;
            case R.id.courts:
                Intent intent = new Intent(SecondActivity.this, CourtActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //===============================================================
    // IMPLEMENTED METHODS - ON CLICK LISTENER
    //===============================================================

    @Override
    public void onClick(View v) {
        Bundle c = new Bundle();
        c.putString("carORbike", carORbike);
        Intent it2 = new Intent(SecondActivity.this, ThirdActivity.class);
        switch (v.getId()) {
            case R.id.bt1:
                offenceRelatedto = "parking";
                c.putString("offenceRelatedto", offenceRelatedto);
                it2.putExtras(c);
                startActivity(it2);
                break;
            case R.id.bt2:
                offenceRelatedto = "driving";
                c.putString("offenceRelatedto", offenceRelatedto);
                it2.putExtras(c);
                startActivity(it2);
                break;
            case R.id.bt3:
                offenceRelatedto = "documents";
                c.putString("offenceRelatedto", offenceRelatedto);
                it2.putExtras(c);
                startActivity(it2);
                break;
            case R.id.bt4:
                offenceRelatedto = "pollution";
                c.putString("offenceRelatedto", offenceRelatedto);
                it2.putExtras(c);
                startActivity(it2);
                break;
            case R.id.bt5:
                offenceRelatedto = "vehicles";
                c.putString("offenceRelatedto", offenceRelatedto);
                it2.putExtras(c);
                startActivity(it2);
                break;
            case R.id.bt6:
                offenceRelatedto = "goods";
                c.putString("offenceRelatedto", offenceRelatedto);
                it2.putExtras(c);
                startActivity(it2);
                break;

        }

    }

}
