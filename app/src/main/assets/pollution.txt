[{
	"offence" : "Fixing multi-toned/shrill horn",
	"section" : "119 CMVR 190(2) MVA",
	"fine" : "500",
	"corb" : "car & bike",
    "category" : "pollution"
},
{
	"offence" : "Silencer/muffler making noise",
	"section" : "CMVR 120 190(2) MVA",
	"fine" : "500",
	"corb" : "car & bike",
    "category" : "pollution"
},
{
	"offence" : "Smoky exhaust",
	"section" : "115 CMVR 190(2) MVA",
	"fine" : "500",
	"corb" : "car & bike",
    "category" : "pollution"
},
{
	"offence" : "Using horn in silence zone",
	"section" : "21(ii) RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "pollution"
},
{
	"offence" : "Pollution Not Under Control (PUC)",
	"section" : "99(1)(a)/177 DMVR",
	"fine" : "1000",
	"corb" : "car & bike",
    "category" : "pollution"
}]