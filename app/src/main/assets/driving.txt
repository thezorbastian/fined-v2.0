[{
	"offence" : "Driving against Police signal",
	"section" : "119 r/w 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Drunken driving or Driving under the influence of drugs",
	"section" : "185 MVA",
	"fine" : "Whoever, while driving, or attempting to drive, a Motor Vehicle-
(a) Has in his blood, alcohol exceeding 30 mg. /per.100 ml. of blood detected in a test by a breath analyzer,
(b) Is under the influence of a drug to such an extent as to be incapable of exercising proper control over the vehicle,
Shall be punishable for the first offence with imprisonment for a term which may extend to six months or with fine which may extend to Rs. 2000/-,
or with both and for a second or subsequent offence if committed within 3 years of the commission if the previous similar offence, 
with imprisonment for a term which may extend to 2 years or with fine which may extend to Rs. 3000/-, or with both.",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Driving in the centre/failed to keep left",
	"section" : "2 RRR r/w 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Overtaking dangerously",
	"section" : "6 (a) RRR r/w 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Failing to give way/permit overtaking",
	"section" : "7 RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Failing to slowdown at intersection",
	"section" : "8 RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Failing to take precaution while taking a turn",
	"section" : "3 RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Failing to give signal",
	"section" : "121 RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Reversing negligently",
	"section" : "233 MMVR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Disobeying traffic signal",
	"section" : "239 MMVR 22(b) RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Disobeying manual traffic signal",
	"section" : "239 MMVR 22(a) RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Driving against one-way",
	"section" : "17 (i) RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Failing to keep left of traffic island",
	"section" : "2 RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Exceeding speed limit", 
	"section" : "112-183 MVA",
	"fine" : "200. 
Whoever drives a motor vehicle in contravention of the speed limits referred to in section 112 shall be punishable with a fine which may extend to Rs. 400/- 
if having been previously convicted of an offence under this sub-section is again convicted of an offence under this sub-section in which case fine may extend to Rs. 1000/-",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Taking 'U' turn during prohibited hours",
	"section" : "12 RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Disobeying reasonable directions of police officer in uniform",
	"section" : "119 MVA 22(a) RRR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Leaving vehicle in dangerous position",
	"section" : "122 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Carrying persons on footboard",
	"section" : "123-177 MVA",
	"fine" : "100",
	"corb" : "car",
    "category" : "driving"
},
{
	"offence" : "Carrying persons causing obstruction to the driver",
	"section" : "125-177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Leaving vehicle with idling engine",
	"section" : "126-177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Disobeying traffic sign board",
	"section" : "22(b) RRR 239 MMVR 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Driving without illuminating rear number plate",
	"section" : "CMVR 105 (2) (ii) 177 MVA",
	"fine" : "100",
	"corb" : "car & bike",
    "category" : "driving"
},
{
	"offence" : "Driving without helmet",
	"section" : "129 r/w177 MVA",
	"fine" : "100",
	"corb" : "bike",
    "category" : "driving"
},
{
	"offence" : "Seat belts not fastened",
	"section" : "138(3) CMVR 177 MVA",
	"fine" : "100",
	"corb" : "car",
    "category" : "driving"
},
{
	"offence" : "Trippling.",
	"section" : "128/177 MVA",
	"fine" : "100",
	"corb" : "bike",
    "category" : "driving"
},
{
	"offence" : "Using Mobile Phone while Driving.",
	"section" : "184 MVA",
	"fine" : "Up to 1000/-",
	"corb" : "car & bike",
    "category" : "driving"
}]